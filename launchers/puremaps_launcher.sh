#!/bin/sh
# title="$icon_gps PureMaps"
# Author:magdesign
# Description: this script enables geoclue agent and starts puremaps
# Notes: v0.21 requires: pure-maps, geoclue

# enable gps (get is optional)
mmcli -m any --location-enable-gps-raw
mmcli -m any --location-enable-gps-nmea
mmcli -m any --location-get

sleep 1

#launch geoclue
/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
sleep 1
notify-send "geoclue running"

# osmscout-server launcher for offline navigation
# osmscout-server &

#launch pure-maps 
pure-maps

exit
