#!/bin/sh
# title="$icon_bat_c_3 Powerlevel"
# Author: magdesign
# License: MIT
# Description:Shows battery level and state
# Note: v0.2
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# read percentage
PERCENTAGE=$(upower -d | awk '/percentage/; /^$/ {exit}' | sed 's/[^0-9]//g')

# read state
STATE=$(upower -d | awk '/state/; /^$/ {exit}' |  sed 's/state://g;s/ //g')

# time to empty
EMPTY=$(upower -d | awk '/ time to empty/; /^$/ {exit}' | sed 's/ time to empty://g;s/ //g')

# time to full  
FULL=$(upower -d | awk '/ time to full/; /^$/ {exit}' | sed 's/ time to full://g;s/ //g')

# charging current
CURRENT1=$(cat /sys/class/power_supply/pmi8998-charger/hwmon23/curr1_input)

if [ "$STATE" == "charging" ]; then
        notify-send "$PERCENTAGE%"" Battery ""$STATE -> $CURRENT1 mAh" "$FULL left"
elif [[ "$PERCENTAGE" -lt 15 ]]; then
	notify-send -u critical "$PERCENTAGE""%"" Battery LOW and ""$STATE" "$EMPTY"" left"
else        notify-send "$PERCENTAGE""%"" Battery ""$STATE" "$EMPTY"" left"

fi

exit

