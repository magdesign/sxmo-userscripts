#!/bin/sh
# title="$icon_dsk Diskspace"
# Author: magdesign
# License: MIT
# Description: shows diskspace
# Note: v0.1
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

FREESPACE=$(df -h | awk '/root/ { print "Free: " $4 }')
USEDSPACE=$(df -h | awk '/root/ { print "Used: " $3 }')
OFSPACE=$(df -h | awk '/root/{print $2}')
PERCENTAGE=$(df -h | awk '/root/{print $5}' | sed 's/%//g')


#Go red when only 10% is left
if [ $PERCENTAGE -lt 10 ]; then
	notify-send -u critical "$FREESPACE" "$USEDSPACE of $OFSPACE"
else 
	notify-send  "$FREESPACE" "$USEDSPACE of $OFSPACE"
fi
exit
