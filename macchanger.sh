#!/bin/sh
# title="$icon_net MacChanger"
# Description: creates a new macaddress on wifi interface
# Requrements: bash macchanger
# Author: magdesign
# Notes: v0.2 Usefull on public hotspots
# License: MIT
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# check requirements
if command -v macchanger >/dev/null 2>&1 ;then
echo "macchanger installed"
else
  notify-send --urgency=critical "Please install macchanger"
  exit 1
fi

# ask for sudo and execute
sxmo_terminal.sh -t "Enter password" -- sh -c '$(sudo rfkill block wlan && sleep 1 && sudo macchanger wlan0 -ar && sudo rfkill unblock wlan)'

# show maccaddress
OUTPUT=$(macchanger -s wlan0 | sed -e 's/(unknown)//g')

# notify
notify-send "mac address changed" "$OUTPUT"

exit 0
