#!/bin/bash
# title="$icon_pst GIFscreen"
# Author: magdesign
# License: MIT
# Description: Decodes qr-code of se4lected area
# Requirements: bash imagemagick
# Notes:v0.2  Works only on sway/wayland
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# to do: variable duration (bmenu) or stop with volume button 

# Check if ImageMagick is installed
if command -v convert >/dev/null 2>&1 ;then
echo "ImageMagick installed"
else
  notify-send --urgency=critical "Please install ImageMagick"
  exit 1
fi

# change only duration and framerate:
duration=5
total=$((6 * duration))
framerate=6
effective=$((1 / $framerate))
# save resulting gif to:
location=~/Pictures
# tempdir to 
mkdir /tmp/gif/
DIR=/tmp/gif
# countdown until start:
precount=5

# set wanted area:
area="$(slurp -o)"

countdown() {
  seconds=$1

  while [ $seconds -gt 0 ]; do
    notify-send -t 300 "$seconds seconds until.." "$duration seconds recording"
    sleep 1
    seconds=$((seconds - 1))
  done

  notify-send -t 500 --urgency=critical "live"
}

# execute countdown
countdown $precount

#Capture continuous screenshots
for ((i = 0; i < total; i++)); do
  # Format the file name with leading zeros
  filename=$(printf "%05d.png" "$i")

  # Capture the screenshot using 'grim'
  grim  -g "$area" "$DIR/$filename"

  # Wait for 1/6th of a second
  sleep $effective
done

# message
notify-send -t 2000 --urgency=low "done... now converting"

# convert to animated gif
convert -delay $effective -loop 0 $DIR/*.png "$location/$(date +%Y-%m-%d-%H-%M-%S).gif"

notify-send --urgency=critical "done...see:" "$location"

# cleanup
rm -rf /tmp/gif/
exit
