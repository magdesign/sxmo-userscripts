#!/bin/sh
# title="icon_usr ChatBot"
# Author: magdesign
# Description: launches the ollama2 container
# Notes: v0.1 requires: ollama2 container installed

# check if container exists
if podman inspect ollama > /dev/null 2>&1; then
    echo "ollama container found"
else
    echo "no ollama container installed"
fi

# launch and execute
podman container start ollama 
sleep 1
sxmo_terminal.sh podman exec -it ollama ollama run llama3
# replace llama with: llama2-uncensored; mixtral; phi3
# stop after closing terminal
podman container stop ollama

# todo: temperature control with emergency kill
# cat /sys/class/thermal/thermal_zone*/temp | awk '{sum += $1} END {printf("%.2f", sum/NR/1000)}'

exit
