#!/bin/sh
# title="$icon_gps GPS enable"
# Description: enables gps
# Author: magdesign
# License: MIT
# Notes: v0.2 removed sudo
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# ask for sudo and execute
#sxmo_terminal.sh -t "Enter password" -- sh -c '$(sudo mmcli -m any --location-enable-gps-nmea && sudo mmcli -m any --location-enable-gps-raw && sudo mmcli -m any --location-get)'

mmcli -m any --location-enable-gps-nmea
mmcli -m any --location-enable-gps-raw 
mmcli -m any --location-get 


# notify
notify-send  "GPS enabled"

exit 0
