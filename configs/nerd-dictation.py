# User configuration file typically located at `~/.config/nerd-dictation/nerd-dictation.py`
import re, subprocess, sys

# if we wanna speak commands
def run_command_or_exit_on_failure(cmd):
    try:
        subprocess.check_output(cmd)
            # i dont know how, but here we should be able to execute keystrokes somehow
#            ("enter", echo key 'enter' | dotool)
            # mabe we can just add them like
#            "execute": "echo key 'enter' | dotool",        
    # Don't catch other kinds of exceptions as they should never happen
    # and can be considered a severe error which doesn't need to be made "user friendly".
    except FileNotFoundError as ex:
        print("Command {!r} not found: {!s}".format(cmd[0], ex))
        sys.exit(1)


# -----------------------------------------------------------------------------
# Replace Multiple Words

TEXT_REPLACE_REGEX = (
    ("\\b" "data type" "\\b", "data-type"),
    ("\\b" "copy on write" "\\b", "copy-on-write"),
    ("\\b" "key word" "\\b", "keyword"),
)
TEXT_REPLACE_REGEX = tuple(
    (re.compile(match), replacement) for (match, replacement) in TEXT_REPLACE_REGEX
)

# -----------------------------------------------------------------------------
# Replace Single Words

# VOSK-API doesn't use capitals anywhere so they have to be explicit added in.
WORD_REPLACE = {
    "i": "I",
    "api": "API",
    "linux": "Linux",
    # It's also possible to ignore words entirely.
    "hum": "",
    "um": "",
    "uh": "",
}

# Regular expressions allow partial words to be replaced.
WORD_REPLACE_REGEX = (("^i'(.*)", "I'\\1"),)
WORD_REPLACE_REGEX = tuple(
    (re.compile(match), replacement) for (match, replacement) in WORD_REPLACE_REGEX
)

# -----------------------------------------------------------------------------
# Add Punctuation

CLOSING_PUNCTUATION = {  # space added after
    "space mark": " ",
    "espace": " ",
    #
    "period": ".",
    "point final": ".",
    "dot": ".",
    #
    "comma": ",",
    "virgule": ",",
    "coma": ",",
    #
    "question mark": "?",
    "point d'interrogation": "?",
    "exclamation mark": "!",
    "point d'exclamation": "!",
}

OTHER_PUNCTUATION = {
    #
    "open quote": '"',
    "close quote": '"',
    #
    "new line": "\r",
    #
    "ampersand": "&",
    "asterisk": "*",
    #
    "back slash": "\\",
    "forward slash": "/",
    #
    "dash": "-",  # same for fr
    #
    "open parenthesis": "(",
    "close parenthesis": ")",
    #
    "open bracket": "[",
    "close bracket": "]",
    #
    "greater than": ">",
    "lower than": "<",
}

OPENING_PUNCTUATION = {
    "semi-colon": ";",
    "point virgule": ";",
    #
    "colon": ":",
    "double point": ":",
}

# -----------------------------------------------------------------------------
# Main Processing Function


def nerd_dictation_process(text):
    # run_command_or_exit_on_failure(cmd)

    for match, replacement in TEXT_REPLACE_REGEX:
        text = match.sub(replacement, text)

    for match, replacement in CLOSING_PUNCTUATION.items():
        text = text.replace(" " + match, replacement).replace(
            match, replacement
        )  # + " "

    for match, replacement in OTHER_PUNCTUATION.items():
        text = text.replace(match, replacement)

    for match, replacement in OPENING_PUNCTUATION.items():
        text = text.replace(
            match + " ", replacement
        )  # .replace(match, replacement) + " "

    # Add sentence case
    def apply_sentence_case(match):
        return match.group(0).upper()

    REGEX_SENTENCE = re.compile(r"[\.?!] [a-z]")
    text = REGEX_SENTENCE.sub(apply_sentence_case, text)

    if "majuscule" in text.lower():
        text = (
            text.capitalize()
            .replace("Majuscules ", "")
            .replace("Majuscule ", "")
            .replace("majuscules ", "")
            .replace("majuscule ", "")
            .replace("majuscules", "")
            .replace("majuscule", "")
        )

    words = text.split(" ")

    for i, w in enumerate(words):
        w_init = w
        w_test = WORD_REPLACE.get(w)
        if w_test is not None:
            w = w_test
        if w_init == w:
            for match, replacement in WORD_REPLACE_REGEX:
                w_test = match.sub(replacement, w)
                if w_test != w:
                    w = w_test
                    break

        words[i] = w

    # Strip any words that were replaced with empty strings.
    words[:] = [w for w in words if w]

    text = " ".join(words)

    # Fix any new lines with a trailing space
    text = text.replace("\r ", "\r")

    return text
