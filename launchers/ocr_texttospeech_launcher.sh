#!/bin/sh
# title="icon_spk TextToSpeech"
# Author: magdesign
# Description: text-to-speech output from region of interest
# Notes: v0.3 requires: piper-tts
# check requirements

if command -v piper &> /dev/null
then
    echo "piper-tts is installed"
else
  notify-send --urgency=critical "install piper-tts and a model first"
  exit 1
fi

# Check if it is running
if pgrep -f piper> /dev/null
then
    echo "stop it"
	pkill -f piper
	sxmobar -d piper
else
	echo "start ocr_roi first"
	$HOME/sxmo-userscripts/ocr_roi.sh
    echo "start text-to-speech"
    # is not running, start it 
    
	sxmobar -f green -a piper 60 "󰀄"
	#execute here
wl-paste | /usr/bin/piper --model $HOME/piper/models/en/en_US-lessac-medium.onnx --config $HOME/piper/models/en/en_en_US_lessac_medium_en_US-lessac-medium.onnx.json --output-raw | aplay -r 22050 -f S16_LE -t raw -	
	#remove icon when finished speaking
	sxmobar -d piper

fi
