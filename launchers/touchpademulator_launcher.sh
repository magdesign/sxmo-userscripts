#!/bin/sh
# title="icon_usr Touchpad"
# Author: magdesign
# Description: toggles touchpad emulator
# Notes: v0.2 requires: patched TouchpadEmulator in /usr/bin

# check requirements
if command -v TouchpadEmulator >/dev/null 2>&1 ;then
echo "is installed"
else
  notify-send --urgency=critical "Please install patched TouchpadEmulator"
  exit 1
fi

# Check if 'TouchpadEmulator' is running
if pgrep -f TouchpadEmulator > /dev/null
then
    echo "Kill 'TouchpadEmulator'"
	pkill -f TouchpadEmulator
		sxmobar -d touch 
else
    echo "Process 'TouchpadEmulator' is not running"
    # is not running, start it
	# and check if screen is rotated 
	if [[ $( swaymsg -t get_outputs | grep transform | awk '{print $2}') == '"normal",' ]]; then
	echo "run normal with option 0"
	sxmobar -f green -a touch 68 󰍽 
	/usr/bin/TouchpadEmulator 0
	exit
	else
	echo "is rotated, run with 1"
	sxmobar -f green -a touch 68 󰍽 
	/usr/bin/TouchpadEmulator 1
	fi
fi
