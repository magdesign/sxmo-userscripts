#!/bin/bash
# title="$icon_pst QRscreen"
# Author: magdesign
# License: MIT4
# Description: Decodes qr-code of selected area
# Requirements: bash wl-clipboard zxing
# Notes:v0.2.1  Works only in sway/wayland
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# check requirements
if command -v ZXingReader >/dev/null 2>&1 ;then
echo "ok"
else
  notify-send --urgency=critical "Please install zxing"
  exit 1
fi
if command -v wl-paste >/dev/null 2>&1 ;then
echo "ok"
else
  notify-send --urgency=critical "Please install wl-clipboard"
  exit 1
fi
# end of requirement check

# temp dir/files
DIR=/tmp/qr_tmp.png
DIR2=/tmp/qr_tmp.txt

# create a screenshot of wanted area:
area="$(slurp -o)"
grim -g "$area" "$DIR"

# sends image to zbarimg to decode 
ZXingReader $DIR | awk '/^Text:/ {print $2}'| awk '{gsub(/^"|"$/,"")}1' > $DIR2

# check if we found something
if [[ -s $DIR2 ]]; then

# displays extracted qr content
notify-send "$(cat /tmp/qr_tmp.txt)"
# and copy text to clipboard
cat /tmp/qr_tmp.txt | wl-copy

else
notify-send --urgency=critical "nothing found"
fi

# cleanup
rm /tmp/qr_tmp.txt
rm /tmp/qr_tmp.png

exit

