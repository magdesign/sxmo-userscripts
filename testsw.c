#include <stdio.h>
#include <linux/input.h>
#include <fcntl.h>

// to query position of alert slider on op6
// compile before use: gcc testsw.c -o testsw
    
int main (int argc, char **argv)                                                               
{   
    int fd;
    int abs[6] = {0};
    
    if ((fd = open("/dev/input/event1", O_RDONLY)) < 0) {
        perror("evtest");
    }  
    
    ioctl(fd, EVIOCGABS(34), abs);
    printf("%d\n", abs[0]);
    
    return abs[0];
}
