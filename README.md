# This repo is no longer active, use:
# https://codeberg.org/magdesign/sxmop6

# Custom sxmo userscripts, because we can.

**you need: `sudo apk add bash vnstat zxing tesseract-ocr tesseract-ocr-data-eng wl-clipboard imagemagick wf-recorder`**

clone the whole repo into your home folder so you can use the scripts described in https://wiki.postmarketos.org/wiki/User:Magdesign

there is many many stuff :)

Put them into: ` ~/.config/sxmo/userscripts/` 

sxmo_screenshot.sh goes into `/usr/bin/`

Make each executable with: `sudo chmod +x`

There is now an installer to fix most annoying sxmo/op6 bugs:
https://codeberg.org/magdesign/sxmop6

to do: 
sms deleter e.g. select via dmenu out of texts menu, 'Delete a Text'
(rm .local/share/sxmo/modem/<sender>/sms.txt)

Njoy and drop me a line on Masto:
https://fosstodon.org/@pocketvj



Get info [here](https://wiki.postmarketos.org/wiki/User:Magdesign)

![screenshot](images/scripts_sml.png "userscripts")


## datausage_wlan.sh
shows how many MB is used.

make sure to set vnstat to run on boot:

`sudo apk add vnstat`

`sudo rc-update add vnstatd`

![screenshot](images/data_sml.png "datausage_wlan.sh")

## datausage_reset.sh
resets the counter to zero.

## powerlevel.sh
display powerlevel in % and if its charging or discharging as notify-send popup.
on op6 it also shows charging amperes.

![screenshot of powerlevel](images/power_sml.png "powerlevel.sh")

## macchanger.sh
creates a random mac address on wlan adaptor.

![screenshot macchanger](images/mac_sml.png "macchanger.sh")

## gps_enable.sh
enables gps catcher.

## ssh_toggler.sh
enables or disables ssh.

## ocr_roi.sh
capture text on screen and extracts it to the clipboard.
(if you want to start it via a gesture, you might have to add it to /usr/bin)

![screenshot](images/ocr_sml.png "ocr_roi.sh")

## qr_roi.sh
decode qr codes on screen and extracts it to the clipboard.

![screenshot](images/qr_sml.png "qr_roi.sh")

## gif_roi.sh
selection screenrecording to gif
in sway.

![screenshot](images/airplane_sml.png "airplane.sh")

## airplane.sh
toggle GSM modem state, ignoring wifi.

## screenrecorder.sh
records a selection of your screen to *.mp4 stop command is currently hardcoded to op6 volumebuttons instead of sxmo_inputhandler.sh
to do: fix stop button, add a maximum timeout.

## torch.sh
toggles the torch from On to both On to Off.

## sxmo_screenshot.sh
fixed version of fullscreen shot.

# Configs

![screenshot](images/a_sml.png "krita_phone")

## krita_phone.kws
minimal layout to use krita on phone.

Krita Menu => Window => Workspaces => Import Workspace => select op6.2

![screenshot](images/kdenlive_sml.png "kdenlive_phone")

## phn.kdenlivelayout
layout for kdenlive on mobile

Go to View => Manage Layouts and import the phn layout.

Make sure to delete all other layouts to get space for the menu.

Turn your phone layout to landscape (you might prior switch scaling to 2) and go to:

Settings => Configure Toolbars => TimelineToolbar

In the right window, remove:

    Track Menu
    Timeline Edit Menu
    Separator

Remove everything after the Timecode 00:00:00:00

After the Spacer Tool, add:

    Favorite Effects
    Delete Selected Item
    Insert Track...

![screenshot](images/conky_sml.png "conky")

## conky.conf
setting to display datausage and powerlevel on homescreen.
goes into: ~/.config/sxmo/conky.conf

# Launchers

## osmin_launcher.sh
launches osmin with gos & geoclue.

## puremaps_launcher.sh
launches puremaps with gps & geoclue.
