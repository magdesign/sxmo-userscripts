#!/bin/sh
# title="$icon_gps Osmin"
# Author: magdesign
# Description: this script enables geoclue agent and starts osmin
# Notes: v0.21, Requires: osmin, geoclue

# enable gps (get is optional)
mmcli -m any --location-enable-gps-raw
mmcli -m any --location-enable-gps-nmea
# mmcli -m any --location-get
sleep 1

# launch geoclue
/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
sleep 1
notify-send "geoclue running"

#launch osmin
osmin 

exit
