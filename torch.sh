# !/bin/sh
# title="$icon_fll Torch"
# Author: magdesign
# License: Mit
# Note: v0.1 torch toggler
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# Define path of Led / Torch
WHITE=/sys/class/leds/white:flash/brightness  
YELLOW=/sys/class/leds/yellow:flash/brightness

if [ "$(cat $WHITE)" == "1" ]
    then
        if [ "$(cat $YELLOW)" == "1" ]
            then
                echo 0 > $WHITE
                echo 0 > $YELLOW
                echo turn both torches off
        else
            echo 1 > $YELLOW
            echo turn yellow torch on
       fi
    else
        echo 1 > $WHITE
        echo turn white torch on
fi
