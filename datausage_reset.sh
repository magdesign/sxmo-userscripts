#!/bin/sh
# title="$icon_modem_disabled zero datausage"
# Description: resets the datausage counter
# Author: magdesign
# License: MIT
# Notes: v0.1
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# ask for sudo and execute
sxmo_terminal.sh -t "Enter password" -- sh -c '$(sudo rc-service vnstatd stop && sleep 3 && sudo rm -rf /var/lib/vnstat/vnstat.db && sudo rc-service vnstatd start)'

# notify
#notify-send --urgency=critical "datausage set to 0"

exit 0
