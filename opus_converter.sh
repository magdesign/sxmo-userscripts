#!/bin/bash
# title="$icon_aud opus-converter"
# Author: magdesign
# Description: converts all audio files in folder to opus
# Notes: v0.1
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# echeck for ffmpeg
if ! command -v ffmpeg &> /dev/null; then 
    echo "Error: ffmpeg is not installed" 
    exit 1 
fi


# Loop through all files, including those with whitespaces and special characters
for file in *; do 
    # Check if the file is a regular file with supported audio formats
    if [[ -f "$file" && $file =~ .+\.(mp3|wav|ogg|aac|m4a)$ ]]; then 
        # Get the filename without the extension
        filenameNoExt="${file%.*}"
        # Convert the audio file to opus with 128 kps
        ffmpeg -i "$file" -c:a libopus -b:a 128k "$filenameNoExt.opus"  
    fi
done



# Remove all files ending with mp3, wav, ogg, aac, m4a
#find . -type f \( -name "*.mp3" -o -name "*.wav" -o -name "*.ogg" -o -name "*.aac" -o -name "*.m4a" \) -exec rm {} +
